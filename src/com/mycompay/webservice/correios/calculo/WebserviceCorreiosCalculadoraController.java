/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompay.webservice.correios.calculo;

import com.mycompay.webservice.correios.calculo.util.Alerts;
import java.math.BigDecimal;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.tempuri.CServico;

public class WebserviceCorreiosCalculadoraController {
    
    @FXML
    private TextField txValor;

    @FXML
    private TextField txPeso;

    @FXML
    private Label lbResult;
    
    @FXML
    private Label lbResult2;
    
    @FXML
    private Label lbResult3;
    
    @FXML
    private Label lbResult4;

    @FXML
    private Button btnCalculo;

    @FXML
    private TextField txServico;

    @FXML
    private TextField txMP;

    @FXML
    private TextField txCEPOrigem;

    @FXML
    private TextField txFormato;

    @FXML
    private TextField txRec;

    @FXML
    private TextField txDiam;

    @FXML
    private TextField txCEPDestino;

    @FXML
    private TextField txComp;

    @FXML
    private TextField txAlt;

    @FXML
    private TextField txLarg;
    
    @FXML
    void btnCalcularFreteAction() {
        org.tempuri.CalcPrecoPrazoWS service = new org.tempuri.CalcPrecoPrazoWS();
        org.tempuri.CalcPrecoPrazoWSSoap port = service.getCalcPrecoPrazoWSSoap();      

        try{
            String servico = txServico.getText();
            String cepOrigem = txCEPOrigem.getText();
            String cepDestino = txCEPDestino.getText();
            String peso = txPeso.getText();
            int formato = Integer.parseInt(txFormato.getText());
            BigDecimal comprimento = new BigDecimal(txComp.getText());
            BigDecimal altura = new BigDecimal(txAlt.getText());
            BigDecimal largura = new BigDecimal(txLarg.getText());
            BigDecimal diametro = new BigDecimal(txDiam.getText());
            String maoPropria = txMP.getText();
            BigDecimal valorDeclarado = new BigDecimal(txValor.getText());
            String recebimento = txRec.getText();
            org.tempuri.CResultado resultado = port.calcPrecoPrazo("", "", servico, cepOrigem, cepDestino, peso, formato, comprimento, altura, largura, diametro, maoPropria, valorDeclarado, recebimento);
            List<CServico> listaServico = resultado.getServicos().getCServico();
            String nomeServico = "";
            int codigoServico = 0;
            for(int i = 0; i < listaServico.size(); i++){
                codigoServico = listaServico.get(i).getCodigo();
                if(codigoServico == 04510){
                    nomeServico = "PAC";
                }else if(codigoServico == 04014){
                    nomeServico = "SEDEX";
                }
                lbResult.setText("Valor: " + listaServico.get(i).getValor());
                lbResult2.setText("Prazo: " + listaServico.get(i).getPrazoEntrega() + " dias");
                lbResult3.setText("Observação: " + listaServico.get(i).getMsgErro());
            }
                        
        }catch(NumberFormatException e){
            Alerts.showAlertt("Error", "Parse error", e.getMessage(), Alert.AlertType.ERROR);
        }
    }

}
