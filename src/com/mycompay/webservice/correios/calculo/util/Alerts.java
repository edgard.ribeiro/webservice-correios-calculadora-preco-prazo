package com.mycompay.webservice.correios.calculo.util;

import javafx.scene.control.Alert;

/**
 *
 * @author Edgard Monção
 */
public class Alerts {
    public static void showAlertt(String title, String header, String content, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.show();
    }
    
}
